package com.for_comprehension.E05;

import org.junit.Test;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.Supplier;

import static java.time.Duration.ofSeconds;
import static java.util.concurrent.CompletableFuture.supplyAsync;

public class CompletableFutureUtilsTest {

    @Test(expected = CompletionException.class)
    public void example_1() throws Exception {

        CompletableFuture<Integer> cf1 = supplyAsync(withDelay(5, ofSeconds(3)));
        CompletableFuture<Integer> cf2 = supplyAsync(withDelay(4, ofSeconds(5)));
        CompletableFuture<Integer> cf3 = supplyAsync(() -> {
            throw new NullPointerException();
        });

        List<Integer> join = CompletableFutureUtils.allOf(cf1, cf2, cf3).join();
    }


    private static <T> Supplier<T> withDelay(T value, Duration delay) {
        return () -> {
            try {
                Thread.sleep(delay.toMillis());
            } catch (InterruptedException e) {
            }

            return value;
        };
    }

}
