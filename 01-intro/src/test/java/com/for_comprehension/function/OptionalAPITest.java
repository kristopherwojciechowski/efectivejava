package com.for_comprehension.function;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class OptionalAPITest {
    @Test
    public void example_1() throws Exception {
        Optional<String> name = getName(42);

        // create
        Optional<Integer> nullable = Optional.ofNullable(null);
        Optional<Integer> nonnullable = Optional.of(42);
        Optional<Integer> empty = Optional.empty();

        // transform
        Optional<String> uppercased = name.map(String::toUpperCase);
        Optional<String> flattened = name.flatMap(s -> getName(1));
        Optional<String> filtered = name.filter(s -> s.contains("John"));

        // unpack
        String result0 = name.get(); // only if you know what you're doing
        String result1 = name.orElse("default");
        String result2 = name.orElseGet(() -> "default");
        String result3 = name.orElseThrow(IllegalStateException::new);
    }

    @Test
    public void example_2() throws Exception {
        Optional<String> name = getName(42);

        List<String> strings = name
          .map(OptionalAPITest::lookupAddress)
          .orElseGet(() -> lookupFallbackAddress());

        System.out.println(strings);
    }

    private static List<String> lookupAddress(String name) {
        return Collections.singletonList("Address1");
    }

    private static List<String> lookupFallbackAddress() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }

        return Arrays.asList("a1", "a2", "a3");
    }

    private static Optional<String> getName(long id) {
        return Optional.ofNullable("John");
    }
}
