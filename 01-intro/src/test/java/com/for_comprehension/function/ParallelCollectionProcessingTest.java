package com.for_comprehension.function;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.for_comprehension.function.PrefixedThreadFactory.prefixed;

public class ParallelCollectionProcessingTest {

    @Test
    public void example_0() throws Exception {
        List<Integer> collect = Stream.iterate(0, i -> i + 1)
          .limit(100)
          .parallel()
          .map(multiply())
          .collect(Collectors.toList());

        System.out.println(collect);
    }

    @Test
    public void example_1() throws Exception {
        // Executor/ExecutorService - podstawowe interfejsy (ale jeszcze nie pule wątków)
        // ThreadPoolExecutor/ForkJoinPool - pule wątków
        // Executors - fabryki do puli wątków

        // Zasady:
        // 1. Nazywamy pule wątków
        // 2. Ograniczamy rozmiar kolejki

        ExecutorService executor = Executors.newFixedThreadPool(10);
        Future<String> result = executor.submit(() -> {
            return "ble - " + Thread.currentThread().getName();
        });

        String s = result.get();

        System.out.println(s);
    }

    @Test
    public void example_2() throws Exception {
        List<Integer> ints = Stream.iterate(0, i -> i + 1)
          .limit(100).collect(Collectors.toList());

        ExecutorService executor = defineThreadPool();

        runInParallel(ints, executor);
    }

    private ExecutorService defineThreadPool() {
        return new ThreadPoolExecutor(2, 50,
          1, TimeUnit.MINUTES,
          new ArrayBlockingQueue<>(50),
          prefixed("users"));
    }

    private void runInParallel(List<Integer> ints, ExecutorService executor) {
        List<Future<Integer>> futures = new ArrayList<>();
        for (Integer e : ints) {
            Future<Integer> f = executor.submit(() -> multiply().apply(e));
            futures.add(f);
        }

        List<Integer> collect = futures.stream()
          .map(f -> {
              try {
                  return f.get();
              } catch (InterruptedException e) {
                  Thread.currentThread().interrupt();
                  throw new RuntimeException(e);
              } catch (ExecutionException e) {
                  throw new RuntimeException(e);
              }
          }).collect(Collectors.toList());

        System.out.println(collect);
    }

    private static Function<Integer, Integer> multiply() {
        return i -> {
            try {
                Thread.sleep(2000);
                System.out.println(i + "-" + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
            return i * 2;
        };
    }
}
