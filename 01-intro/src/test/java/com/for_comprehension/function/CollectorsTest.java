package com.for_comprehension.function;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class CollectorsTest {

    @Test
    public void example_1() throws Exception {
        List<String> integers = Arrays.asList("a", "bb", "ccc", "dd");

        Map<Integer, List<String>> r1 = integers.stream()
          .collect(groupingBy(String::length));

        Map<Integer, String> r2 = integers.stream()
          .collect(groupingBy(String::length, Collectors.joining()));

        System.out.println(r1);
        System.out.println(r2);
    }

    @Test
    public void example_2() throws Exception {
        List<String> integers = Arrays.asList("a", "bb", "ccc", "dd");

        Set<String> collect = integers.stream()
          .collect(
            collectingAndThen(
              toList(),
              HashSet::new));

        List<String> immutableList = integers.stream()
          .collect(Collectors.collectingAndThen(
            toList(),
            list -> Collections.unmodifiableList(list)));

        System.out.println(immutableList);
    }

    @Test
    public void example_3() throws Exception {
        List<String> strings = Arrays.asList("a", "bb", "ccc", "dd");

        Map<Boolean, List<String>> collect = strings.stream()
          .collect(Collectors.partitioningBy(i -> i.length() == 2));

        System.out.println(collect);
    }

    @Test
    public void example_4() throws Exception {
        List<String> strings = Arrays.asList("a", "bb", "ccc", "bb", "dd", "");

        Map<String, Integer> collect = strings.stream()
          .collect(toMap(e -> e, String::length, (v1, v2) -> v1));

        System.out.println(collect);
    }
}
