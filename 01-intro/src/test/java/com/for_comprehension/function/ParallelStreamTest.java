package com.for_comprehension.function;

import org.junit.Test;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParallelStreamTest {

    @Test
    public void example_1() throws Exception {

        // gdzie są uruchamiane taski? na jakiej puli? czy nowa pula jest tworzona?
        // jaki parallelism?
        // jaki jest rozmiar kolejki przed pulą wątków?
        // jakie są parametry puli? jaki jest maksymalny rozmiar? jaki jest minimalny rozmiar? co się dzieje w przypadku kiedy pula jest przepełniona?

        List<Integer> collect = Stream.iterate(0, i -> i + 1)
          .limit(100)
          .parallel()
          .map(duplicate())
          .collect(Collectors.toList());

        System.out.println(collect);
    }

    private static Function<Integer, Integer> duplicate() {
        return i -> {
            try {
                Thread.sleep(100);
                System.out.println(i + "-" + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
            return i * 2;
        };
    }
}
