package com.for_comprehension.function;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class HelloStreamTest {

    @Test
    public void example_1() {
        // imperative
        Optional<Integer> result = Optional.empty();
        for (Integer i : Arrays.asList(1, 2, 3)) {
            Integer integer = i * 2;
            if (integer > 2) {
                result = Optional.of(integer);
                break;
            }
        }

        // declarative
        Optional<Integer> result2 = Stream.of(1, 2, 3)
          .map(i -> i * 2)       // 1
          .filter(i -> i > 2)    // 2
          .findFirst();          // 3
    }

    @Test
    public void example_2() {
        Stream<Integer> integerStream = Stream.of(1, 2, 3);
        Stream<Integer> infinite = Stream.generate(() -> 42);
        Stream<Integer> iterate = Stream.iterate(0, i -> i + 1);

        Stream<Integer> stream = Arrays.asList(1, 2, 3).stream();
        Stream<Map.Entry<Object, Object>> entryStream = Collections.emptyMap().entrySet().stream();

        Stream<Integer> concat = Stream.concat(Stream.of(1), Stream.of(2));
    }

    @Test(expected = IllegalStateException.class)
    public void example_3() {
        Stream<Integer> integerStream = Stream.of(1);

        integerStream.forEach(x -> System.out.println(x));
        integerStream.forEach(x -> System.out
          .println(x)); // java.lang.IllegalStateException: stream has already been operated upon or closed
    }

    @Test
    public void example_4() {
        List<Integer> integers = Arrays.asList(1);

        integers.stream().forEach(System.out::println);
        integers.stream().forEach(System.out::println);
    }

    @Test
    public void example_5() {
        List<String> integers = Arrays.asList("a", "bb", "cccc", "dd");

        List<String> list = integers.stream()
          .collect(Collectors.toList());

        Set<String> set = integers.stream()
          .collect(Collectors.toSet());

        TreeSet<String> treeset = integers.stream()
          .collect(Collectors.toCollection(() -> new TreeSet<>()));

        Map<String, Integer> map = integers.stream()
          .collect(Collectors.toMap(i -> i, i -> i.length()));
    }

    @Test
    public void example_6() {
        boolean r1 = Stream.of("a", "bb", "ccc")
          .allMatch(s -> s.length() < 10);

        boolean r2 = Stream.of("a", "bb", "ccc")
          .noneMatch(s -> s.length() < 10);

        boolean r3 = Stream.of("a", "bb", "ccc")
          .anyMatch(s -> s.length() < 2);

        System.out.println(r1);
        System.out.println(r2);
        System.out.println(r3);
    }

    @Test
    public void example_7() {
        Stream.of("a", "bb", "ccc", "bb")
          .distinct()
          .forEach(System.out::println);
    }

    @Test
    public void example_8() {
        Stream.of("a", "bb", "ccccc", "dddd")
          .sorted(Comparator.comparingInt(s -> s.length()))
          .forEach(System.out::println);
    }

    @Test
    public void example_9() {
        Stream.of("a", "bb", "ccccc", "dddd")
          .min(Comparator.comparingInt(s -> s.length()))
          .ifPresent(System.out::println);

        Stream.of("a", "bb", "ccccc", "dddd")
          .max(Comparator.comparingInt(s -> s.length()))
          .ifPresent(System.out::println);
    }

    @Test
    public void example_10() {
        Stream.of("a", "bb", "ccccc", "dddd")
          .skip(1)
          .limit(2)
          .forEach(System.out::println);
    }

    @Test
    public void example_11() {
        Arrays.asList(Arrays.asList(1), Arrays.asList(2, 3), Arrays.asList(4))
          .stream()
          .forEach(System.out::println);

        Arrays.asList(Arrays.asList(1), Arrays.asList(2, 3), Arrays.asList(4))
          .stream()
          .flatMap(i -> i.stream())
          .forEach(System.out::println);
    }

    @Test
    public void example_12() {
        Optional<String> reduce1 = Stream.of("a", "bb", "ccccc", "dddd")
          .map(s -> s.toUpperCase())
          .reduce((s, s2) -> s + s2);

        String reduce2 = Stream.of("a", "bb", "ccccc", "dddd")
          .map(s -> s.toUpperCase())
          .reduce("", (s, s2) -> s + s2);

        System.out.println(reduce1);
        System.out.println(reduce2);
    }

    @Test
    public void example_13() {
        OptionalDouble average = Stream.of(17, 2, 3)
          .mapToInt(i -> i)
          .average();

        System.out.println(average);

        IntStream primitive = IntStream.of(1, 2, 3);
        Stream<Integer> boxed = IntStream.of(1, 2, 3).boxed();
    }

    @Test
    public void example_14() {
        List<String> urls = Arrays.asList("asdads", "as12");

        List<InputStream> results = new ArrayList<>();

        try {
            for (String url : urls) {
                results.add(new URL(url).openStream());
            }
        } catch (MalformedURLException e) {
            // ...
        } catch (IOException e) {
            // ...
        }

        try {
            List<InputStream> collect = urls.stream()
              .map(s -> {
                  URL url;
                  try {
                      url = new URL(s);
                  } catch (MalformedURLException e) {
                      throw new RuntimeException(e);
                  }
                  return url;
              })
              .filter(i -> true)
              .map(url -> {
                  try {
                      return url.openStream();
                  } catch (IOException e) {
                      throw new RuntimeException(e);
                  }
              }).collect(Collectors.toList());
        } catch (Exception e) {
            if (e.getCause() instanceof IOException) {
                //..
            }

            if (e.getCause() instanceof MalformedURLException) {
                //..
            }
        }
    }

    @Test
    public void example_15() throws Exception {
        String foo = null;

        if (foo != null) {
            System.out.println(foo);
        }

        Optional.ofNullable(foo).ifPresent(System.out::println);
    }

    @Test
    public void example_16() throws Exception {
        Optional<String> asd = Optional.ofNullable("asd");

        Optional<String> maybeDefault = Optional.ofNullable("default");

        if (!asd.isPresent()) {
            asd = maybeDefault;
        }
    }

    @Test(expected = NumberFormatException.class)
    public void example_17() throws Exception {
        List<String> asd = Arrays.asList("asd");
        asd.stream()
          .map(i -> Long.parseLong(i))
          .collect(Collectors.toList());

        List<Long> results = new ArrayList<>();
        for (String s : asd) {
            results.add(Long.parseLong(s));
        }
    }
}
