package com.for_comprehension.function;

import org.junit.Test;

import java.util.Optional;
import java.util.function.Function;

public class OptionalTest {

    @Test
    public void example_1() throws Exception {
        Optional<String> name = getName(42);
        Optional<String> postfix = name.map(s -> "-postfix");
        Optional<String> uppercased = postfix.map(String::toUpperCase);
        Optional<String> nonempty = uppercased.filter(s -> !s.isEmpty());
        String r1 = nonempty.orElse("DEFAULT");

        // ----

        // declarative
        String r2 = getName(42)
          .map(s -> s + "-postfix")
          .map(String::toUpperCase)
          .filter(s -> !s.isEmpty())
          .orElse("DEFAULT");

        // imperative
        String result;
        Optional<String> optionalName = getName(42);
        if (optionalName.isPresent()) {
            String s = optionalName.get() + "-postfix";
            String s1 = s.toUpperCase();
            if (!s1.isEmpty()) {
                result = s1;
            } else {
                result = "DEFAULT";
            }
        }
    }

    interface Monad<T> {
        <R> Monad<R> flatMap(Function<T, Monad<R>> function);
    }

    private static Optional<String> getName(long id) {
        return Optional.ofNullable("John");
    }
}
