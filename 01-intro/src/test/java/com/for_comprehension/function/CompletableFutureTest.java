package com.for_comprehension.function;

import org.junit.Test;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Supplier;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class CompletableFutureTest {

    @Test
    public void example_0() throws ExecutionException {
        ExecutorService e = null;
        try {
            e = Executors.newSingleThreadExecutor();
            Future<String> result = e.submit(() -> "Hello from another thread!");

            String s = result.get() + "-postfix";
            System.out.println(s);
        } catch (InterruptedException interruptedException) {
            Thread.currentThread().interrupt();
            throw new RuntimeException();
        } finally {
            if (e != null) {
                e.shutdown();
            }
        }
    }

    @Test
    public void example_1_manual_completion() {
        CompletableFuture<String> cf1 = new CompletableFuture<>();
        cf1.complete("42");

        String join = cf1.join();

        System.out.println(join);
    }

    @Test
    public void example_2() {
        ExecutorService e = null;
        try {
            e = Executors.newSingleThreadExecutor();
            CompletableFuture<String> result = supplyAsync(helloTask(), e);

            result
              .thenApply(s -> s + " ::" + Thread.currentThread().getName())
              .thenAccept(System.out::println)
              .join();
        } finally {
            if (e != null) {
                e.shutdown();
            }
        }
    }

    @Test
    public void example_3() {
        ExecutorService executor = Executors.newFixedThreadPool(10);

        CompletableFuture<String> f = supplyAsync(helloTask("1"), executor);

        f.thenApply(i -> i + "postfix");                 // map(i -> i + "postfix")
        f.thenAccept(System.out::println);               // forEach(System.out::println)
        f.thenCompose(i -> new CompletableFuture<>());   // flatMap(i -> new CompletableFuture<>())

        f.thenRun(() -> System.out.println("Hi!"));
    }

    @Test
    public void example_4() {
        ExecutorService executor = Executors.newFixedThreadPool(10);

        CompletableFuture<String> f1 = supplyAsync(helloTask("1"), executor);
        CompletableFuture<String> f2 = supplyAsync(helloTask("2"), executor);

        f1.applyToEither(f2, i -> i) // w środku jest wynik f1 albo f2 w zależności od tego, który zwrócił pierwszy
          .thenAccept(System.out::println)
          .join();
    }

    @Test
    public void example_5() {
        ExecutorService executor = Executors.newFixedThreadPool(10);

        CompletableFuture<String> f1 = supplyAsync(helloTask("1"), executor);
        CompletableFuture<String> f2 = supplyAsync(helloTask("2"), executor);

        f1.thenCombine(f2, (s, s2) -> s + s2) // czekamy na dwa future i przetwarzamy dwa wyniki
          .thenAccept(System.out::println)
          .join();
    }

    @Test
    public void example_6() {
        ExecutorService executor = Executors.newFixedThreadPool(10);

        CompletableFuture<String> f1 = supplyAsync(() -> {
            throw new IllegalStateException(helloTask("1").get());
        }, executor);

        f1.exceptionally(ex -> "error: " + ex.getMessage())
          .thenAccept(System.out::println)
          .join();
    }

    @Test
    public void example_7() {
        ExecutorService exec = Executors.newFixedThreadPool(10);

        CompletableFuture<String> f1 = supplyAsync(helloTask("1"), exec);
        CompletableFuture<String> f2 = supplyAsync(helloTask("2"), exec);
        CompletableFuture<String> f3 = supplyAsync(helloTask("3"), exec);
        CompletableFuture<String> f4 = supplyAsync(helloTask("4"), exec);

        CompletableFuture.anyOf(f1, f2, f3, f4).thenAccept(System.out::println).join();

        CompletableFuture<String> cfs = new CompletableFuture<>();
        cfs.complete("42");

        CompletableFuture<Integer> cfi = new CompletableFuture<>();
        cfi.complete(42);

        CompletableFuture<Object> objectCompletableFuture = CompletableFuture.anyOf(cfs, cfi);
    }

    @Test
    public void example_8() {
        ExecutorService exec = Executors.newFixedThreadPool(10);

        CompletableFuture<String> f1 = supplyAsync(helloTask("1"), exec);
        CompletableFuture<String> f2 = supplyAsync(helloTask("2"), exec);
        CompletableFuture<String> f3 = supplyAsync(helloTask("3"), exec);
        CompletableFuture<String> f4 = supplyAsync(helloTask("4"), exec);

        CompletableFuture<Void> voidCompletableFuture = CompletableFuture.allOf(f1, f2, f3, f4);
    }

    private static Supplier<String> helloTask() {
        return () -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException interruptedException) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(interruptedException);
            }
            return "Hello from another thread!";
        };
    }

    private static Supplier<String> helloTask(String value) {
        return () -> {
            try {
                Thread.sleep(Math.abs(new Random().nextInt(10000)));
            } catch (InterruptedException interruptedException) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(interruptedException);
            }
            return value;
        };
    }
}
