package com.for_comprehension.E02;

import java.util.Optional;

class OptionalsACL {
    public static void main(String[] args) {
        OptionalsACL optionalsACL = new OptionalsACL(new OptionalsRefactor());

        Optional<OptionalsRefactor.Person> person = optionalsACL.findPerson(42);
    }

    private final OptionalsRefactor delegate;

    OptionalsACL(OptionalsRefactor delegate) {
        this.delegate = delegate;
    }

    public Optional<OptionalsRefactor.Person> findPerson(int id) {
        return Optional.ofNullable(delegate.findPerson(id));
    }

    public Optional<String> findAddress(OptionalsRefactor.Person person) {
        return Optional.ofNullable(delegate.findAddress(person));
    }

    public Optional<String> findAddressById(int id) {
        return Optional.ofNullable(delegate.findAddressById(id));
    }
}
