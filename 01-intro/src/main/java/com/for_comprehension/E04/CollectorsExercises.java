package com.for_comprehension.E04;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

class CollectorsExercises {

    /**
     * Collect elements to a {@link List} instance
     */
    static Function<List<String>, List<String>> L1_toList() {
        return list -> {
            return list.stream().collect(toList());
            // return new ArrayList<>(list);
        };
    }

    /**
     * Collect elements to a {@link LinkedList} instance
     */
    static Function<List<String>, LinkedList<String>> L2_toLinkedList() {
        return list -> {
            return list.stream().collect(toCollection(LinkedList::new));
            // return new LinkedList<>(list);
        };
    }

    /**
     * Collect elements to a {@link List} wrapped in {@link Collections#unmodifiableList(List)} instance
     */
    static Function<List<String>, List<String>> L3_unmodifiable() {
        return list -> {
            return list.stream()
              .collect(Collectors.collectingAndThen(
                toList(),
                Collections::unmodifiableList));
        };
    }

    /**
     * Collect elements to a {@link Map} instance with uppercased elements as keys and their corresponding lengths as values
     * and resolve potential collisions
     */
    static Function<List<String>, Map<String, Integer>> L4_toMap() {
        return list -> {
            return list.stream()
              .collect(toMap(
                s -> s.toUpperCase(),
                s -> s.length(),
                (v1, v2) -> v1));
        };
    }


    /**
     * Collect elements to a {@link TreeMap} instance with elements as keys and their corresponding lengths as values
     * and resolve potential collisions by picking any of the strings
     */
    static Function<List<String>, TreeMap<String, Integer>> L5_toTreeMap() {
        return list -> {
            return list.stream()
              .collect(toMap(
                e -> e,
                e -> e.length(),
                (v1, v2) -> v1,
                () -> new TreeMap<>()));
        };
    }

    /**
     * Collect Map elements to a JSON String instance
     * {@link Collectors#joining(CharSequence, CharSequence, CharSequence)}
     */
    static Function<Map<String, String>, String> L6_toJson() {
        return input -> {
            return input.entrySet().stream()
              .map(e -> String.format("\"%s\":\"%s\"", e.getKey(), e.getValue()))
              .collect(Collectors.joining(",", "{", "}"));
        };
    }

    /**
     * Group Strings of the same length
     * {@link Collectors#groupingBy(Function)}
     */
    static Function<List<String>, Map<Integer, List<String>>> L7_groupStrings() {
        return input -> {
            return input.stream()
              .collect(Collectors.groupingBy(String::length));
        };
    }

    /**
     * Group Strings of the same length to a {@link TreeMap}
     */
    static Function<List<String>, TreeMap<Integer, List<String>>> L8_groupStrings() {
        return input -> {
            return input.stream()
              .collect(Collectors.groupingBy(String::length, TreeMap::new, toList()));
        };
    }

    /**
     * Group Strings of the same length into a comma-delimited String
     * {@link Collectors#groupingBy(Function)}
     */
    static Function<List<String>, Map<Integer, String>> L9_groupStrings() {
        return input -> input.stream()
          .collect(Collectors.groupingBy(String::length, Collectors.joining(",")));
    }
}
