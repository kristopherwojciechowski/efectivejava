package com.for_comprehension.E04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;

public class UnmodifiableListCollector<T> implements Collector<T, ArrayList<T>, List<T>> {

    static <T> Collector<T, ?, List<T>> toUnmodifiableList() {
        return new UnmodifiableListCollector<>();
    }

    /**
     * Throws {@link UnsupportedOperationException} since {@link java.util.stream.Collectors#toCollection(Supplier)}
     * assumes that the target is mutable
     */
    public static void main(String[] args) {
        List<Integer> list = Stream.of(42).collect(toUnmodifiableList());
        System.out.println(list);
    }

    @Override
    public Supplier<ArrayList<T>> supplier() {
        return () -> new ArrayList<T>();
    }

    @Override
    public BiConsumer<ArrayList<T>, T> accumulator() {
        return (acc, t) -> {
            acc.add(t);
        };
    }

    @Override
    public BinaryOperator<ArrayList<T>> combiner() {
        return (acc1, acc2) -> {
            acc1.addAll(acc2);
            return acc1;
        };
    }

    @Override
    public Function<ArrayList<T>, List<T>> finisher() {
        return Collections::unmodifiableList;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return new HashSet<>(Collections.singletonList(Characteristics.UNORDERED));
    }
}
