package com.for_comprehension.function;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

class Scheduler<T> extends Thread {

    public static void main(String[] args) throws InterruptedException {
        Scheduler<Integer> consumer = new Scheduler<>();
        consumer.add(1);
        consumer.add(2);
        consumer.add(3);

        consumer.start();

        Thread.sleep(10000);

        System.out.println("interrupting...");
        consumer.interrupt();
    }

    private final BlockingQueue<T> queue = new ArrayBlockingQueue<>(100);

    public void add(T e) {
        queue.add(e);
    }
   /*
   @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println(queue.take());
            }
        } catch (InterruptedException ignored) {
            // wyjątek można zignorować, bo już wyskoczyliśmy z pętli
        }
    }
    */

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println(queue.take());
            } catch (InterruptedException interruptedException) {
                // musimy wyskoczyć z pętli ręcznie
                break;
            }
        }
    }

    /*
    catch (InterruptedException interruptedException) {
        Thread.currentThread().interrupt();
        throw new RuntimeException(interruptedException);
    }
    */
}
