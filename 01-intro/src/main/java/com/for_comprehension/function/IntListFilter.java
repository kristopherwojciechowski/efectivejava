package com.for_comprehension.function;

import java.util.Arrays;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class IntListFilter {

    public static void main(String[] args) {
        List<Integer> ints = Stream.iterate(0, i -> i + 1)
          .limit(100000)
          .collect(Collectors.toList());

        List<Integer> results = new IntListFilter(
          i -> i > 0,
          i -> i < 100,
          i -> i % 2 == 1,
          i -> i % 5 == 0
        ).filter(ints);

        System.out.println(results);
    }

    private final List<IntPredicate> predicates;

    IntListFilter(IntPredicate... predicates) {
        this.predicates = Arrays.asList(predicates);
    }

    public List<Integer> filter(List<Integer> list) {
        IntPredicate reduced = predicates.stream()
          .reduce(a -> true, IntPredicate::and);

        return list.stream()
          .mapToInt(i -> i)
          .filter(reduced)
          .boxed()
          .collect(Collectors.toList());
    }
}
