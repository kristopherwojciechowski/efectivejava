package com.for_comprehension.function;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

class PrefixedThreadFactory implements ThreadFactory {

    private final String prefix;
    private final ThreadFactory threadFactory = Executors.defaultThreadFactory();

    private PrefixedThreadFactory(String prefix) {
        this.prefix = prefix;
    }

    public static ThreadFactory prefixed(String prefix) {
        return new PrefixedThreadFactory(prefix);
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread thread = threadFactory.newThread(r);
        thread.setName(prefix + "-" + thread.getName());
        return thread;
    }
}
