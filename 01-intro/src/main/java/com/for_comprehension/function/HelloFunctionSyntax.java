package com.for_comprehension.function;

import java.util.function.BinaryOperator;
import java.util.function.Function;

class HelloFunctionSyntax {

    public static void main(String[] args) {
        Function<Integer, Integer> f1 = i -> i + 1;
        Function<Integer, Integer> f2 = (i) -> i + 1;
        Function<Integer, Integer> f3 = (i) -> {
            return i + 1;
        };

        Function<Integer, Integer> f4 = (Integer i) -> {
            return i + 1;
        };

        Function<Integer, Integer> f5 = new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer integer) {
                return integer + 1;
            }
        };

        BinaryOperator<Integer> b1 = Integer::sum;
    }
}
