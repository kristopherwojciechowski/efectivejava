package com.for_comprehension.function;

import java.util.concurrent.Callable;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

class HelloFunctionalInterfaces {

    public static void main(String[] args) {
        Function<Integer, Integer> f1 = i -> i + 1;
        BiFunction<Integer, Integer, Integer> bf = (i1, i2) -> i1 + i2;
        TriFunction<Integer, Integer, Integer, Integer> tf = (i1, i2, i3) -> i1 + i2 + i3;

        Consumer<Integer> c1 = i -> System.out.println(i); // Function<Integer, Void>
        // primitive -> IntConsumer
        Supplier<Integer> s1 = () -> 42;                   // Function<Void, Integer>
        // primitive -> IntSupplier
        Predicate<Integer> p1 = i -> i % 2 == 1;           // Function<Integer, Boolean>
        // primitive -> IntPredicate
        UnaryOperator<Integer> u1 = i -> i + 1;            // Function<Integer, Integer>
        // primitive -> IntUnaryOperator
        BinaryOperator<Integer> b1 = (i1, i2) -> i1 + i2;  // BiFunction<Integer, Integer, Integer>
        // primitive -> IntBinaryOperator

        Runnable r1 = () -> {};
        Callable<Integer> ca1 = () -> 42;
    }

    @FunctionalInterface
    private interface TriFunction<T1, T2, T3, R> {
        R apply(T1 t1, T2 t2, T3 t3);
    }
}
