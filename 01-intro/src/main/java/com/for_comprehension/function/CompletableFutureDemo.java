package com.for_comprehension.function;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class CompletableFutureDemo {

    public static void main(String[] args) {
        ExecutorService e = null;
        try {
            e = Executors.newSingleThreadExecutor(r -> {
                Thread thread = new Thread(r);
                thread.setDaemon(false);
                return thread;
            });
            CompletableFuture<String> result = CompletableFuture
              .supplyAsync(() -> {
                  try {
                      Thread.sleep(10000);
                  } catch (InterruptedException interruptedException) {
                      interruptedException.printStackTrace();
                  }
                  return "Hello from another thread!";}, e);

            result
              .thenApply(s -> s + "-postfix")
              .thenAccept(System.out::println);

            System.out.println("Doing some other work");

        } finally {
            if (e != null) {
                e.shutdown();
            }
        }
    }
}
