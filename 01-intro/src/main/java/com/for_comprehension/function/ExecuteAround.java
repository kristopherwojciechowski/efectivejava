package com.for_comprehension.function;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

class ExecuteAround {

    public static void main(String[] args) {

        List<String> result = logged(timed(() -> {
            return fetchUsers();
        }));
    }

    private static <T> T logged(Supplier<T> supplier) {
        System.out.println("entering a method at " + Instant.now());
        T t = supplier.get();
        System.out.println("exiting a method at " + Instant.now());
        return t;
    }

    private static <T> Supplier<T> timed(Supplier<T> supplier) {
        return () -> {
            Instant before = Instant.now();
            T result = supplier.get();
            Instant after = Instant.now();
            System.out
              .println("took " + Duration.between(before, after).toMillis() + " millis to get: " + result.toString());
            return result;
        };
    }

    public static List<String> fetchUsers() {
        try {
            Thread.sleep(Math.abs(new Random().nextInt(2000)));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException();
        }
        return Arrays.asList("John", "Adam");
    }
}
