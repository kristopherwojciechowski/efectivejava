package com.for_comprehension.function;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;

import static java.util.stream.Collector.Characteristics.UNORDERED;

public class CustomCollector {

    public static void main(String[] args) {

        String collect = Stream.of("a", "b", "c")
          .collect(toCSV());

        System.out.println(collect);
    }

    public static <T> Collector<T, ?, String> toCSV() {
        return new CSVCollector<>();
    }

    public static class CSVCollector<T> implements Collector<T, StringBuilder, String> {
        @Override
        public Supplier<StringBuilder> supplier() {
            return () -> new StringBuilder();
        }

        @Override
        public BiConsumer<StringBuilder, T> accumulator() {
            return (stringBuilder, t) -> {
                if (stringBuilder.toString().isEmpty()) {
                    stringBuilder.append(t);
                } else {
                    stringBuilder.append(",").append(t.toString());
                }
            };
        }

        @Override
        public BinaryOperator<StringBuilder> combiner() {
            return (sb1, sb2) -> sb1.append(",").append(sb2);
        }

        @Override
        public Function<StringBuilder, String> finisher() {
            return StringBuilder::toString;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return new HashSet<>(Collections.singletonList(UNORDERED));
        }
    }
}
