package com.for_comprehension.function;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

abstract class TimerGoF<T> {

    public static void main(String[] args) {
        List<String> strings = new FetchUsersTask().runWithTimer();
    }

    protected abstract T runInternal();

    public T runWithTimer() {
        System.out.println("entering a method at " + Instant.now());
        Instant before = Instant.now();
        T t = runInternal();
        Instant after = Instant.now();
        System.out.println("exiting a method at " + Instant.now());

        System.out.println(Duration.between(before, after).toMillis() + " millis");
        return t;
    }

    static class FetchUsersTask extends TimerGoF<List<String>> {
        @Override
        protected List<String> runInternal() {
            return ExecuteAround.fetchUsers();
        }
    }
}
