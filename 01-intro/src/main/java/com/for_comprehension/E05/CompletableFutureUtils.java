package com.for_comprehension.E05;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.util.stream.Collectors.toList;

final class CompletableFutureUtils {

    private CompletableFutureUtils() {
    }

    @SafeVarargs
    public static <T> CompletableFuture<List<T>> allOf(CompletableFuture<T>... cfs) {
        CompletableFuture<Void> futures = CompletableFuture.allOf(cfs);

        CompletableFuture<List<T>> result = futures
          .thenApply(__ -> Arrays.stream(cfs)
            .map(CompletableFuture::join)
            .collect(toList()));

        for (CompletableFuture<T> cf : cfs) {
            cf.handle((t, throwable) -> {
                if (throwable != null) {
                    result.completeExceptionally(throwable);
                }
                return null;
            });
        }

        return result;
    }

    public static <T> CompletableFuture<List<T>> allOf(Collection<CompletableFuture<T>> cfs) {
        return allOf(cfs.toArray(new CompletableFuture[0]));
    }

    public static <T> CompletableFuture<T> anyOf(CompletableFuture<T>... cfs) {
        return CompletableFuture.anyOf(cfs).thenApply(i -> (T) i);
    }

    public static <T> CompletableFuture<T> anyOf(Collection<CompletableFuture<T>> cfs) {
        return anyOf(cfs.toArray(new CompletableFuture[0]));
    }
}
