package com.pivovarit.movies;

import com.pivovarit.movies.rental.api.MovieDto;
import org.assertj.core.groups.Tuple;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("default")
public class MovieTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private RestTemplate restTemplate;

    @Before
    public void before() {
        assertThat(getAllMovies()).isEmpty();
    }

    @Test
    void should_add_movie() {
        MovieDto movie = new MovieDto(1, "1", "NEW", "");

        restTemplate.postForObject("http://localhost:{port}/api/movies/", movie, Void.class, port);

        List<MovieDto> movies = getAllMovies();

        assertThat(movies)
          .hasSize(1)
          .extracting("id", "title", "type")
          .contains(Tuple.tuple(1L, "1", "NEW"));
    }

    @Test
    void should_get_all_movies() {
        MovieDto movie1 = new MovieDto(1, "1", "NEW", "");
        MovieDto movie2 = new MovieDto(2, "2", "OLD", "");
        MovieDto movie3 = new MovieDto(3, "3", "REGULAR", "");
        restTemplate.postForObject("http://localhost:{port}/api/movies/", movie1, Void.class, port);
        restTemplate.postForObject("http://localhost:{port}/api/movies/", movie2, Void.class, port);
        restTemplate.postForObject("http://localhost:{port}/api/movies/", movie3, Void.class, port);

        assertThat(getAllMovies())
          .hasSize(3)
          .extracting("id", "title", "type")
          .contains(Tuple.tuple(1L, "1", "NEW"))
          .contains(Tuple.tuple(2L, "2", "OLD"))
          .contains(Tuple.tuple(3L, "3", "REGULAR"));
    }

    @Test
    void should_get_movie_by_id() {
        MovieDto movie = new MovieDto(1, "1", "NEW", "");

        restTemplate.postForObject("http://localhost:{port}/api/movies/", movie, Void.class, port);

        MovieDto result = restTemplate.getForObject("http://localhost:{port}/api/movies/{id}", MovieDto.class, port, 1);

        assertThat(result.getId()).isEqualTo(movie.getId());
        assertThat(result.getTitle()).isEqualTo(movie.getTitle());
        assertThat(result.getType()).isEqualTo(movie.getType());
    }

    @Test
    void should_get_all_movies_by_type() {
        MovieDto movie1 = new MovieDto(1, "1", "NEW", "");
        MovieDto movie2 = new MovieDto(2, "2", "OLD", "");
        MovieDto movie3 = new MovieDto(3, "3", "REGULAR", "");
        restTemplate.postForObject("http://localhost:{port}/api/movies/", movie1, Void.class, port);
        restTemplate.postForObject("http://localhost:{port}/api/movies/", movie2, Void.class, port);
        restTemplate.postForObject("http://localhost:{port}/api/movies/", movie3, Void.class, port);

        assertThat(restTemplate
          .getForObject("http://localhost:{port}/api/movies?type={?}", MovieDto[].class, port, "REGULAR"))
          .hasSize(1)
          .extracting("id", "title", "type")
          .contains(Tuple.tuple(3L, "3", "REGULAR"));
    }

    private List<MovieDto> getAllMovies() {
        return Arrays.asList(restTemplate.getForObject("http://localhost:{port}/api/movies/", MovieDto[].class, port));
    }
}
