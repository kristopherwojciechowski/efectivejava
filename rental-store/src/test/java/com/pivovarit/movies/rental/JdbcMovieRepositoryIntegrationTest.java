package com.pivovarit.movies.rental;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("jdbc")
public class JdbcMovieRepositoryIntegrationTest {

    @Autowired
    public MovieRepository movieRepository;

    @Autowired
    public JdbcTemplate jdbcTemplate;

    @BeforeEach
    void before() {
        jdbcTemplate.update("DELETE FROM movies");
    }

    @Test
    void shouldAddMovie() {
        Movie movie2 = new Movie(new MovieId(2), "2", MovieType.OLD);

        movieRepository.save(movie2);

        assertThat(movieRepository.findById(new MovieId(2)).get().getTitle()).isEqualTo("2");
    }

    @Test
    void shouldFindMovieById() {
        Movie movie = new Movie(new MovieId(1), "1", MovieType.NEW);
        Movie movie2 = new Movie(new MovieId(2), "2", MovieType.OLD);
        Movie movie3 = new Movie(new MovieId(3), "3", MovieType.REGULAR);

        movieRepository.save(movie);
        movieRepository.save(movie2);
        movieRepository.save(movie3);

        assertThat(movieRepository.findById(new MovieId(2)).get().getTitle())
          .isEqualTo("2");
    }

    @Test
    void shouldFindMovieByTitle() {
        Movie movie = new Movie(new MovieId(1), "1", MovieType.NEW);
        Movie movie2 = new Movie(new MovieId(2), "2", MovieType.OLD);
        Movie movie3 = new Movie(new MovieId(3), "3", MovieType.REGULAR);

        movieRepository.save(movie);
        movieRepository.save(movie2);
        movieRepository.save(movie3);

        assertThat(movieRepository.findByTitle("2").get().getId().getId())
          .isEqualTo(2);
    }

    @Test
    void shouldReturnEmptyMovieList() {
        assertThat(movieRepository.findAll()).isEmpty();
    }

    @Test
    void shouldFindAllMovies() {
        Movie movie = new Movie(new MovieId(1), "1", MovieType.NEW);
        Movie movie2 = new Movie(new MovieId(2), "2", MovieType.OLD);
        Movie movie3 = new Movie(new MovieId(3), "3", MovieType.REGULAR);

        movieRepository.save(movie);
        movieRepository.save(movie2);
        movieRepository.save(movie3);

        assertThat(movieRepository.findAll())
          .hasSize(3)
          .contains(movie, movie2, movie3);
    }
}
