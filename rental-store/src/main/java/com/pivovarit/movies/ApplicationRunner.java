package com.pivovarit.movies;

import com.pivovarit.movies.rental.MovieFacade;
import com.pivovarit.movies.rental.api.MovieAddRequest;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
class ApplicationRunner implements CommandLineRunner {

    private final MovieFacade movieFacade;

    ApplicationRunner(MovieFacade movieFacade) {
        this.movieFacade = movieFacade;
    }

    @Override
    public void run(String... args) throws Exception {

        movieFacade.save(new MovieAddRequest(1, "Spiderman", "NEW"));
        movieFacade.save(new MovieAddRequest(2, "Tenet", "NEW"));
        movieFacade.save(new MovieAddRequest(3, "Casablanca", "OLD"));
    }
}
