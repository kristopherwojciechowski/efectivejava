package com.pivovarit.movies;

import com.pivovarit.movies.rental.MovieFacade;
import com.pivovarit.movies.rental.api.MovieAddRequest;
import com.pivovarit.movies.rental.api.MovieDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class MovieController {

    private final MovieFacade movieService;

    public MovieController(MovieFacade movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/movies/{id}")
    public Optional<MovieDto> findById(@PathVariable int id) {
        return movieService.findById(id);
    }

    @PostMapping("/movies")
    public void save(@RequestBody MovieAddRequest movie) {
        System.out.println(movie);
        movieService.save(movie);
    }

    @GetMapping("/movies")
    public Collection<MovieDto> findByType(@RequestParam Optional<String> type) {
        return type.isPresent()
          ? movieService.findAll().stream().filter(m -> m.getType().equals(type.get())).collect(Collectors.toList())
          : movieService.findAll();
    }
}
