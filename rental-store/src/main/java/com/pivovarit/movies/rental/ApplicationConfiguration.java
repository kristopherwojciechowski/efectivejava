package com.pivovarit.movies.rental;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import java.net.URL;
import java.util.List;

@Configuration
class ApplicationConfiguration {

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource ds) {
        return new JdbcTemplate(ds);
    }

    @Bean
    public RestTemplate restTemplate(List<HttpMessageConverter<?>> messageConverters) {
        return new RestTemplate(messageConverters);
    }

    @Bean
    @Profile("prod")
    public MovieDetailsClient movieDetailsClient(
      RestTemplate restTemplate,
      @Value("${movie-details.url}") URL url) {
        return new MovieDetailsClient(restTemplate, url);
    }

    @Bean
    @Profile("!prod")
    public MovieDetailsRepository inmemoryMovieDetailsClient() {
        return new StubMovieDetailsRepository();
    }

    @Bean
    @Profile("!prod")
    public MovieRepository inMemMovieRepository() {
        return new InMemoryMovieRepository();
    }

    @Bean
    @Profile({"prod", "jdbc"})
    MovieRepository movieRepository(JdbcTemplate jdbcTemplate) {
        return new JdbcMovieRepository(jdbcTemplate);
    }

    @Bean
    public MovieFacade movieService(MovieRepository movieRepository, MovieDetailsRepository movieDetailsRepository) {
        return new MovieFacade(movieRepository, movieDetailsRepository, new MovieConverter());
    }
}
