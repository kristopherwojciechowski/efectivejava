package com.pivovarit.movies.rental;

import com.pivovarit.movies.rental.Movie;
import com.pivovarit.movies.rental.MovieId;
import com.pivovarit.movies.rental.MovieType;
import com.pivovarit.movies.rental.api.MovieDto;

class MovieConverter {

    public MovieDto from(Movie movie, String details) {
        return new MovieDto(movie.getId().getId(), movie.getTitle(), movie.getType().toString(), details);
    }

    public Movie from(MovieDto dto) {
        return new Movie(new MovieId(dto.getId()), dto.getTitle(), MovieType.valueOf(dto.getType()));
    }
}
