package com.pivovarit.movies.rental;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

class InMemoryMovieRepository implements MovieRepository {

    private final Map<MovieId, Movie> movies = new ConcurrentHashMap<>();

    @Override
    public MovieId save(Movie movie) {
        movies.put(movie.getId(), movie);
        return movie.getId();
    }

    @Override
    public Collection<Movie> findAll() {
        return movies.values();
    }

    @Override
    public Optional<Movie> findByTitle(String title) {
        return movies.values().stream()
          .filter(m -> m.getTitle().equals(title))
          .findFirst();
    }

    @Override
    public Optional<Movie> findById(MovieId id) {
        return movies.values().stream()
          .filter(m -> m.getId().equals(id))
          .findFirst();
    }
}
