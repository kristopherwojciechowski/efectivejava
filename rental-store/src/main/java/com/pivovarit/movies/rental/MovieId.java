package com.pivovarit.movies.rental;

import com.fasterxml.jackson.annotation.JsonCreator;

class MovieId {

    private final long id;

    @JsonCreator
    public MovieId(long id) {
        this.id = id;
    }

    public long getId() {
        return this.id;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof MovieId)) return false;
        final MovieId other = (MovieId) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof MovieId;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        return result;
    }

    public String toString() {
        return "MovieId(id=" + this.getId() + ")";
    }
}
