package com.pivovarit.movies.rental;

import com.pivovarit.movies.rental.api.MovieAddRequest;
import com.pivovarit.movies.rental.api.MovieDto;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

public class MovieFacade {

    private final MovieRepository repository;
    private final MovieDetailsRepository movieDetailsRepository;
    private final MovieConverter movieConverter;

    public MovieFacade(MovieRepository repository, MovieDetailsRepository movieDetailsRepository, MovieConverter movieConverter) {
        this.repository = repository;
        this.movieDetailsRepository = movieDetailsRepository;
        this.movieConverter = movieConverter;
    }

    public Optional<MovieDto> findById(int id) {
        return repository.findById(new MovieId(id))
          .map(m -> movieConverter.from(m, movieDetailsRepository.findById(m.getId()).getDetails()));
    }

    public MovieId save(MovieAddRequest movie) {
        return repository
          .save(new Movie(new MovieId(movie.getId()), movie.getTitle(), MovieType.valueOf(movie.getType())));
    }

    public Collection<MovieDto> findAll() {
        return repository.findAll().stream()
          .map(m -> movieConverter.from(m, movieDetailsRepository.findById(m.getId()).getDetails()))
          .collect(Collectors.toList());
    }

    public Optional<MovieDto> findByTitle(String title) {
        return repository.findByTitle(title)
          .map(m -> movieConverter.from(m, movieDetailsRepository.findById(m.getId()).getDetails()));
    }

    public Optional<MovieDto> findById(MovieId id) {
        return repository.findById(id)
          .map(m -> movieConverter.from(m, movieDetailsRepository.findById(m.getId()).getDetails()));
    }
}
