package com.pivovarit.movies.rental;

import com.pivovarit.movies.rental.Movie;
import com.pivovarit.movies.rental.MovieId;

import java.util.Collection;
import java.util.Optional;

interface MovieRepository {
    MovieId save(Movie movie);
    Collection<Movie> findAll();
    Optional<Movie> findByTitle(String title);
    Optional<Movie> findById(MovieId id);
}
