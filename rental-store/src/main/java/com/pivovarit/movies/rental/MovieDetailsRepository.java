package com.pivovarit.movies.rental;

interface MovieDetailsRepository {
    MovieDetailsClient.MovieDetails findById(MovieId id);
}
