package com.pivovarit.movies.rental;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.Collection;
import java.util.Optional;

class JdbcMovieRepository implements MovieRepository {

    private final JdbcTemplate jdbcMovieRepository;

    public JdbcMovieRepository(JdbcTemplate jdbcMovieRepository) {
        this.jdbcMovieRepository = jdbcMovieRepository;
    }

    @Override
    public MovieId save(Movie movie) {
        jdbcMovieRepository
          .update("INSERT INTO movies VALUES(?, ?, ?)", movie.getId().getId(), movie.getTitle(), movie.getType()
            .toString());

        return movie.getId();
    }

    @Override
    public Collection<Movie> findAll() {
        return jdbcMovieRepository.query("SELECT * FROM movies", toMovie());
    }

    private static RowMapper<Movie> toMovie() {
        return (rs, rowNum) -> new Movie(new MovieId(rs.getLong("id")), rs.getString("title"), MovieType
          .valueOf(rs.getString("type")));
    }

    @Override
    public Optional<Movie> findByTitle(String title) {
        try {
            return Optional
              .of(jdbcMovieRepository.queryForObject("SELECT * FROM movies m WHERE m.title = ?", toMovie(), title));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Movie> findById(MovieId id) {
        try {
            return Optional
              .of(jdbcMovieRepository.queryForObject("SELECT * FROM movies m WHERE m.id = ?", toMovie(), id.getId()));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }
}
