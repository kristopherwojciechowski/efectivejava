package com.pivovarit.movies.rental;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.pivovarit.movies.rental.MovieDetailsRepository;
import com.pivovarit.movies.rental.MovieId;
import lombok.RequiredArgsConstructor;
import org.springframework.web.client.RestTemplate;

import java.net.URL;

@RequiredArgsConstructor
class MovieDetailsClient implements MovieDetailsRepository {

    private final RestTemplate restTemplate;
    private final URL url;

    @Override
    public MovieDetails findById(MovieId id) {
        return restTemplate.getForObject(url.toString() + "/api/movie-details/{id}", MovieDetails.class, id.getId());
    }

    public static class MovieDetails {
        private final String details;

        @JsonCreator
        public MovieDetails(String details) {
            this.details = details;
        }

        public String getDetails() {
            return details;
        }

        @Override
        public String toString() {
            return "MovieDetails{" +
              "details='" + details + '\'' +
              '}';
        }
    }
}
