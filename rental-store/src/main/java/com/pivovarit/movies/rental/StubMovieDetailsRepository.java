package com.pivovarit.movies.rental;

class StubMovieDetailsRepository implements MovieDetailsRepository {

    @Override
    public MovieDetailsClient.MovieDetails findById(MovieId id) {
        return new MovieDetailsClient.MovieDetails("lorem ipsum");
    }
}
