package com.pivovarit.movies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// curl http://localhost:8081/api/movies

// 1. Podmieniamy MovieDetailsClient na interfejs w MovieFacade
// 2. Na profilu !prod podpinamy zaślepkę/imitację MovieDetailsClient dla wygody
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
